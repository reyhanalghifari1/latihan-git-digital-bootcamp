
public class Programmer extends Person {
	String technology;
	
	public Programmer() {
		super();
	}
	
	public Programmer(String name, String address, String technology) {
		super(name , address);
		this.technology = technology;
	}
	
	void coding() {
		System.out.println("I can create a application useing technology : " + technology + ".");
	}
	
	void hacking() {
		System.out.println("I can hacking a website");
	}
	
	void greeting() {
		super.greeting();//memanggil method greeting dari parent atau person class
		System.out.println("My job si a " + technology + " programmer.");
	}
}
